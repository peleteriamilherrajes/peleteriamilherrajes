setTimeout(function() {
  $("#loader-wrapper").fadeOut(100);
}, 2000);
$(document).ready(function() {
  $("#openModalItem a").click(function(event) {
    var modal = document.getElementById("myModal");
    var img = event.target;
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    if ((modal.style.display = "block")) {
      $("#closeLeft").css("opacity", 0.01);
      var nombre = $("small").text(nombre);
      $("#saludo").html("--" + nombre);
    }
  });
  $("#closeMyModal").click(function() {
    $("#myModal").css("display", "none");
    $("#closeLeft").css("opacity", 2.8);
  });
});
function inicio() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "none");
}
function services() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "none");
}
function noventacinco() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "none");
}
function history() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "none");
}
function clients() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "block");
}
function contact() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  $("#fh5co-menus").css("display", "none");
}
function openNavderecha() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  document.getElementById("myNavderecho").style.width = "100%";
  $("#fh5co-menus").css("display", "none");
  $("#closeRight")
    .css("position", "fixed")
    .css("top", "0");
  document.body.style.overflowY = "hidden";
}
function openNavizquierda() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  document.getElementById("myNavizq").style.width = "100%";
  $("#fh5co-menus").css("display", "none");
  document.getElementById("myNavizq").style.left = "0%";
  document.body.style.overflowY = "hidden";
  if (document.getElementById("nav-p")) {
    document.getElementById("nav-p").style.display = "block";
  }
}
function openNavAbajo() {
  $("body").removeClass("offcanvas-visible");
  $(".js-fh5co-nav-toggle").removeClass("active");
  document.getElementById("myNavabajo").style.height = "100%";
  $("#fh5co-menus").css("display", "none");
  document.getElementById("myNavabajo").style.top = "0%";
  document.body.style.overflowY = "hidden";
}
function closeNavderecho() {
  $("#fh5co-menus").css("display", "block");
  document.getElementById("myNavderecho").style.width = "0%";
  $("#closeRight")
    .css("position", "static")
    .css("top", "0");
  document.body.style.overflowY = "scroll";
}
function closeNavAbajo() {
  $("#fh5co-menus").css("display", "block");
  document.getElementById("myNavabajo").style.top = "100%";
  document.body.style.overflowY = "scroll";
}
function closeNavizq() {
  $("#fh5co-menus").css("display", "block");
  document.getElementById("myNavizq").style.left = "100%";
  document.body.style.overflowY = "scroll";
  document.getElementById("nav-p").style.display = "none";
}
function openNavArriba() {
  document.getElementById("myNavarriba").style.height = "100%";
}
function closeNavArriba() {
  document.getElementById("myNavarriba").style.height = "0%";
}
